import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewGameComponent } from './new-game/new-game.component';
import { GamesComponent } from './games/games.component';
import { HomeComponent } from './home/home.component';
import { GamesByPlatformComponent } from './games/games-by-platform/games-by-platform.component';
import { GameDetailComponent } from './games/game-detail/game-detail.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'new-game', component: NewGameComponent},
  {path: 'list-of-games-by-platform', component: GamesComponent, children: [
      {path: 'personal-computer', component: GamesByPlatformComponent},
      {path: 'sony-playstation', component: GamesByPlatformComponent},
      {path: 'x-box', component: GamesByPlatformComponent},
    ]},
  {path: 'game-detail/:id', component: GameDetailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
