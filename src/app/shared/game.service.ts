import { Game } from './game.model';
import { EventEmitter } from '@angular/core';

export class GameService {
  gamesChange = new EventEmitter<Game[]>();
  gamesByPlatform: Game[] = [];

  private games: Game[] = [
    new Game('Prince of Persia', 'https://secure.diary.ru/userdir/8/4/1/5/841557/32531337.jpg', 'Personal Computer', 'Very interesting game'),
    new Game('Call of Duty', 'https://i.pinimg.com/originals/59/a4/86/59a486b2861213724a9cf5826d834cdd.jpg', 'Personal Computer', 'Action game'),
    new Game ('God of war', 'https://moyamuzika.ru/uploads/thumbs/5/c/a/5ca52c3e2cdf6770d6ba5c61f49d296a.jpg', 'Sony Playstation', 'Adventure game.'),
    new Game('The last of us', 'https://s.4pda.to/DHUIHKfS8AQ3awllD2FEFxEHym5qSWUibPxK.jpg', 'Sony Playstation', 'Adventure game'),
    new Game('Sekiro', 'https://pbs.twimg.com/media/DpZ7ZjQW4AAOE2Y.jpg:large', 'X-box', 'Hardcore game'),
  ];

  getGames() {
    return this.games.slice();
  }

  getGame(index: number) {
    return this.gamesByPlatform[index];
  }

  getGamesByPlatform(platform: string) {
    for (let i = 0; i < this.games.length; i++) {
      if(platform === this.games[i].platform) {
       this.gamesByPlatform.push(this.games[i]);
      }
    }
  }

  addGame(game: Game) {
    this.games.push(game);
    this.gamesChange.emit(this.games);
  }
}
