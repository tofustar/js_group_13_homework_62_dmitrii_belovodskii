import { Component, ElementRef, ViewChild } from '@angular/core';
import { Game } from '../shared/game.model';
import { GameService } from '../shared/game.service';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.css']
})
export class NewGameComponent {

  @ViewChild('nameInput') nameInput!: ElementRef;
  @ViewChild('imageInput') imageInput!: ElementRef;
  @ViewChild('typeSelect') typeSelect!: ElementRef;
  @ViewChild('descriptionTextarea') descriptionTextarea!: ElementRef;

  constructor(private gameService: GameService) { }

  addGame() {
    const name: string = this.nameInput.nativeElement.value;
    const img: string = this.imageInput.nativeElement.value;
    const type: string = this.typeSelect.nativeElement.value;
    const description: string = this.descriptionTextarea.nativeElement.value;

    if (name) {
      const game = new Game(name, img, type, description);
      this.gameService.addGame(game);
    }
  }
}
