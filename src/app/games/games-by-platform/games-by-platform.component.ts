import { Component, OnInit } from '@angular/core';
import { Game } from '../../shared/game.model';
import { GameService } from '../../shared/game.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-games-by-platform',
  templateUrl: './games-by-platform.component.html',
  styleUrls: ['./games-by-platform.component.css']
})
export class GamesByPlatformComponent implements OnInit {
  gamesByPlatform: Game[] = [];

  constructor(private gameService: GameService, private router: Router) {}

  ngOnInit(): void {
    this.gamesByPlatform = this.gameService.gamesByPlatform;
  }

  showInfo(index: number) {
    void this.router.navigate(['/game-detail', index]);
  }
}
