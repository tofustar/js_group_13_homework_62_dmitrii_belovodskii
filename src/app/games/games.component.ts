import { Component, OnInit } from '@angular/core';
import { Game } from '../shared/game.model';
import { GameService } from '../shared/game.service';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit{
  games!: Game[];

  constructor(private gameService: GameService) {}

  ngOnInit() {
    this.games = this.gameService.getGames();
    this.gameService.gamesChange.subscribe((games: Game[]) => {
      this.games = games;
    });
  }

  addToList(platform: string) {
    this.gameService.gamesByPlatform = [];
    this.gameService.getGamesByPlatform(platform);
  }

}
