import { Component, OnInit } from '@angular/core';
import { GameService } from '../../shared/game.service';
import { Game } from '../../shared/game.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css']
})
export class GameDetailComponent implements OnInit {

  game!: Game;

  constructor(private gameService: GameService,private route: ActivatedRoute) {}

  ngOnInit() {
    const gameId = parseInt(this.route.snapshot.params['id']);
    this.game = this.gameService.getGame(gameId);
  }
}
