import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewGameComponent } from './new-game/new-game.component';
import { GamesComponent } from './games/games.component';
import { GameService } from './shared/game.service';
import { FormsModule } from '@angular/forms';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { HomeComponent } from './home/home.component';
import { GamesByPlatformComponent } from './games/games-by-platform/games-by-platform.component';
import { GameDetailComponent } from './games/game-detail/game-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    NewGameComponent,
    GamesComponent,
    ToolbarComponent,
    HomeComponent,
    GamesByPlatformComponent,
    GameDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [GameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
